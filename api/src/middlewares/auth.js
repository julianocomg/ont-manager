import db from '@db/mysql'

function decodeToken(token) {
  try {
    if (!token) {
      return null
    }

    let decodedToken = Buffer.from(token, 'base64').toString('ascii').split(':')

    return {
      login: decodedToken[0],
      password: decodedToken[1]
    }
  } catch(e) {
    return null
  }
}

export default (req, res, next) => {
  let token = decodeToken(req.headers.authorization)

  if (!token) {
    return res.status(401).json({
      message: 'Acesso negado!'
    })
  }

  db.queryAsync(`SELECT login FROM sis_acesso WHERE login = ? AND sha = ?`, [
    token.login,
    token.password
  ])
  .then(customers => {
    if (!customers || !customers[0]) {
      return res.status(401).json({message: 'Acesso negado!'})
    }

    next()
  })
}
