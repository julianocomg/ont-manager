let async = fn => (...args) => fn(...args).catch(e => {
  let req = args[0]
  let res = args[1]

  console.log(e)

  res.status(500).json({
    message: e.message,
    err: e
  })
})

export default async
