import {Router} from 'express'
import auth from '@middlewares/auth'

import createAccessToken from '@api/manager/createAccessToken'
import fetchAvailableOnu from '@api/manager/fetchAvailableOnu'
import createOnu from '@api/manager/createOnu'
import deleteOnu from '@api/manager/deleteOnu'

let Routes = new Router()

Routes.post('/token', createAccessToken)

Routes.get('/onus', auth, fetchAvailableOnu)

Routes.post('/onus', auth, createOnu)

Routes.delete('/onus', auth, deleteOnu)

export default Routes
