import wrap from '@middlewares/async'
import db from '@db/mysql'
import Client from 'ssh2shell'

const SERVER = {
  host: process.env.ONT_HOST,
  userName: process.env.ONT_USER,
  password: process.env.ONT_PASSWORD,
  port: process.env.ONT_PORT
}

export default wrap(async (req, res) => {
  let {body} = req

  let table = 'sis_cliente'
  let loginField = 'login'

  if (body.type === 'additional') {
    table = 'sis_adicional'
    loginField = 'username'
  }

  let login = String(body.login).toUpperCase()

  let customers = await db.queryAsync(
    `SELECT porta_olt, onu_ont, switch FROM ${table} WHERE ${loginField} = ?`,
    [
      login
    ]
  )

  let customer = customers[0]

  if (!customer) {
    return res.status(404).json({message: 'Não existe nenhum usuário cadastrado com esse login.'})
  }

  if (!customer.porta_olt && !customer.onu_ont && !customer.switch) {
    return res.status(409).json({message: 'Este usuário não possui ONU cadastrada.'})
  }

  let slot = customer.porta_olt.split('/')[1]
  let port = customer.porta_olt.split('/')[2]

  var ssh = new Client({
    server: SERVER,
    commands: [
      'enable', 
      'config', 
      `undo service-port port ${customer.porta_olt} ont ${customer.switch}\n\ny`,
      `interface gpon 0/${slot}`,
      `ont delete ${port} ${customer.switch}`
    ]
  })

  ssh.connect(async (e) => {
    if (e.toLowerCase().indexOf('success: 1') === -1) {
      return res.json({message: 'Não foi possível deletar a ONU'})
    }

    await db.queryAsync(
      `UPDATE ${table} SET porta_olt = null, switch = null, onu_ont = null, caixa_herm = null, porta_splitter = null WHERE ${loginField} = ?`,
      [
        login
      ]
    )

    res.json({
      message: 'ONU deletada com sucesso!',
      stack: e
    })
  })
})
