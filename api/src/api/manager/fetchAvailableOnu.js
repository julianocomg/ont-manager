import wrap from '@middlewares/async'
import Client from 'ssh2shell'

const SERVER = {
  host: process.env.ONT_HOST,
  userName: process.env.ONT_USER,
  password: process.env.ONT_PASSWORD,
  port: process.env.ONT_PORT
}

export default wrap(async (req, res) => {
  var ssh = new Client({
    server: SERVER,
    commands: [
      'enable', 
      'config', 
      'display ont autofind all'
    ]
  })

  ssh.connect((output) => {
    if (output.indexOf('Failure:') !== -1) {
      return res.status(404).json({message: 'Nenhuma ONU disponível.'})
    }

    let onus = []

    let data = output.split('----------------------------------------------------------------------------')

    data.forEach(e => {
      if (e.indexOf('Number') === -1) {
        return
      }

      let slot = /(F\/S\/P               :(.*))/.exec(e)[2].trim().split('/')[1]
      let port = /(F\/S\/P               :(.*))/.exec(e)[2].trim().split('/')[2]
      let serial = /(Ont SN              :(.*))/.exec(e)[2].trim().split(' ')[0]
      let number = /(Number              :(.*))/.exec(e)[2].trim()
      let fsp = `0/${slot}/${port}`

      onus.push({
        fsp,
        slot,
        port,
        serial,
        number
      })
    })

    res.json(onus)
  })
})
