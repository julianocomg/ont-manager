import wrap from '@middlewares/async'
import db from '@db/mysql'
import sha256 from 'sha256'

export default wrap(async (req, res) => {
  let {body} = req

  let login = String(body.login)
  let password = sha256(String(body.password))

  let customers = await db.queryAsync(
    `SELECT * FROM sis_acesso WHERE login = ? AND sha = ?`,
    [
      login,
      password
    ]
  )

  let customer = customers[0]

  if (!customer) {
    return res.status(404).json({message: 'Login ou senha incorreto!'})
  }

  res.json({
    access_token: Buffer.from(`${login}:${password}`).toString('base64')
  })
})
