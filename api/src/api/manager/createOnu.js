import wrap from '@middlewares/async'
import db from '@db/mysql'
import Client from 'ssh2shell'

const SERVER = {
  host: process.env.ONT_HOST,
  userName: process.env.ONT_USER,
  password: process.env.ONT_PASSWORD,
  port: process.env.ONT_PORT
}

export default wrap(async (req, res) => {
  let {body} = req

  let table = 'sis_cliente'
  let loginField = 'login'

  if (body.type === 'additional') {
    table = 'sis_adicional'
    loginField = 'username'
  }

  let login = String(body.login).toUpperCase()

  let customers = await db.queryAsync(
    `SELECT porta_olt, onu_ont, switch FROM ${table} WHERE ${loginField} = ?`,
    [
      login
    ]
  )

  let customer = customers[0]

  if (!customer) {
    return res.status(404).json({message: 'Não existe nenhum usuário cadastrado com esse login.'})
  }

  if (customer.porta_olt && customer.onu_ont && customer.switch) {
    return res.status(409).json({message: 'Este usuário já possui ONU cadastrada.'})
  }

  let onu = body.onu

  if (!onu) {
    return res.status(404).json({message: 'Você deve informar a ONU!'})
  }

  var ssh1 = new Client({
    server: SERVER,
    commands: [
      'enable', 
      'config', 
      `interface gpon 0/${onu.slot}`,
      `ont add ${onu.port} sn-auth "${onu.serial}" omci ont-lineprofile-id 10 ont-srvprofile-id 10 desc "${login}"\n\ny`
    ]
  })

  ssh1.connect((e1) => {
    if (e1.indexOf('ONTID') === -1) {
      return res.status(400).json({message: 'Não foi possível autorizar essa ONU.', e1})
    }

    let ontId = /(ONTID :(.*))/.exec(e1)[2]

    var ssh2 = new Client({
      server: SERVER,
      commands: [
        'enable', 
        'config', 
        `interface gpon 0/${onu.slot}`,
        `ont port native-vlan ${onu.port} ${ontId} eth 1 vlan 100 priority 0\n`,
        `quit`,
        `service-port vlan 100 gpon ${onu.fsp} ont ${ontId} gemport 4 multi-service user-vlan 100 tag-transform transparent inbound traffic-table index 10 outbound traffic-table index 10`
      ]
    })

    ssh2.connect(async (e2) => {
      await db.queryAsync(
        `UPDATE ${table} SET porta_olt = ?, switch = ?, onu_ont = ?, caixa_herm = ?, porta_splitter = ? WHERE ${loginField} = ?`,
        [
          onu.fsp,
          ontId,
          onu.serial,
          body.herm,
          body.splitter,
          login
        ]
      )

      res.json({
        message: 'ONU autorizada com sucesso!',
        onu: onu
      })
    })
  })
})
