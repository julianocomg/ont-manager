import express from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'

import managerRoutes from '@routes/manager'

const app = express()

app.use(
  cors(),
  bodyParser.json({ limit: '10mb' })
)

app.use(
  managerRoutes
)

app.listen(8080, () => {
  console.info('ONT Manager API running on port 8080!')
})

process.on('unhandledRejection', (e) => {
  console.error(e)
})