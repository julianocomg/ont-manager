import mysql from 'mysql'

const connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME
})

connection.connect()

connection.queryAsync = (sql = '', args = []) => {
  return new Promise((resolve, reject) => {
    connection.query(sql, args, (error, results) => {
      resolve(results)
    })
  })
}

export default connection
