#!/bin/bash
apt-get update
apt-get install apt-transport-https
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
apt-get update
apt-get install git nodejs yarn
yarn global add forever
git clone https://julianocomg@bitbucket.org/julianocomg/ont-manager.git /opt/mk-auth/admin/ont-manager
cp /opt/mk-auth/admin/ont-manager/addons/add.js /opt/mk-auth/admin/addons/add.js
cd /opt/mk-auth/admin/ont-manager/api
yarn install
cd /opt/mk-auth/admin/ont-manager
echo DB_HOST=localhost >> .env
echo DB_USER=root >> .env
echo DB_PASSWORD=vertrigo >> .env
echo DB_NAME=mkradius >> .env
echo ONT_HOST=$ONT_HOST >> .env
echo ONT_USER=$ONT_USER >> .env
echo ONT_PASSWORD=$ONT_PASSWORD >> .env
echo ONT_PORT=$ONT_PORT >> .env
cd /opt/mk-auth/admin/ont-manager/api
yarn start