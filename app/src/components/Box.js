import React from 'react'
import glamorous, { Div, H1 } from 'glamorous'
import Colors from '@utils/colors'
import Loader from 'react-loading'

const Box = (props) => {
  return (
    <Div
      padding={20}
      borderRadius={4}
      backgroundColor="white"
      width="100%"
      overflow="hidden"
      marginBottom="12px"
      boxShadow="0 7px 14px 0 rgba(50, 93, 70, .1), 0 3px 6px 0 rgba(0,0,0,.07)"
      {...props}
    />
  )
}

Box.Header = glamorous.div({
  marginBottom: 30
})

Box.Title = (props) => {
  return (
    <H1
      fontSize={20}
      color={Colors.TEXT}
      fontWeight={400}
      marginBottom={10}
      marginTop={0}
      display="inline-block"
      {...props}
    />
  )
}

Box.Tools = glamorous.div({
  float: 'right'
})

Box.Loader = ({loading, ...props}) => {
  if (!loading) {
    return null
  }

  return (
    <Div 
      display="inline-block"
      position="relative"
      top={3}
      left={15}
      {...props}>
      <Loader 
        type="spin" 
        color={Colors.OPAQUE}
        width={18}
        height={18}
      />
    </Div>
  )
}

export default Box
