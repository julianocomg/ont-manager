import React from 'react'
import {Div, Form, P} from 'glamorous'
import Box from '@components/Box'
import SelectInput from '@components/SelectInput'
import Colors from '@utils/colors'
import TextInput from '@components/TextInput'
import Button from '@components/Button'
import api from '@api/ont'
import serialize from 'form-serialize'
import ONUSelectInput from '@components/ONUSelectInput'

class AuthorizeONU extends React.Component {
  
  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      error: null,
      success: null
    }
  }

  async onSubmit(e) {
    e.preventDefault()

    let payload = serialize(e.target, {empty: true})

    payload.onu = this.refs.onuSelect.getOnu(payload.onu_index)

    this.setState({loading: true, error: null, success: null})

    api.post('/onus', {
      method: 'POST',
      body: payload
    })
    .then(res => {
      if (res.error) {
        return this.setState({loading: false, error: res.error})
      }

      this.refs.form.reset()
      this.refs.onuSelect.fetchONUS()

      this.setState({
        loading: false, 
        success: `
          <strong>${res.body.message}</strong><br/>
          ${res.body.onu.fsp} [${res.body.onu.serial}]
        `
      })
    })
  }

  render() {
    let {error, success, loading} = this.state

    return (
      <Box>
        <form ref="form" onSubmit={this.onSubmit.bind(this)}>
          <ONUSelectInput
            name="onu_index"
            label="ONU"
            ref="onuSelect"
            required={true}
          />
          <SelectInput
            name="type"
            label="Tipo">
            <option value="main">Usuário principal</option>
            <option value="additional">Usuário adicional</option>
          </SelectInput>
          <TextInput
            required={true}
            name="login"
            label="Login"
          />
          <TextInput
            type="number"
            name="herm"
            label="Nº CTO"
          />
          <TextInput  
            type="number"
            name="splitter"
            label="Splitter"
          />

          <Button
            type="submit"
            loading={loading}
            disabled={loading}
            block={true}>
            Autorizar ONU 
          </Button>

          {!error ? null : 
            <P
              lineHeight="25px"
              textAlign="center"
              color={Colors.ERROR}
              marginBottom={0}
              marginTop={30}
              dangerouslySetInnerHTML={{__html: error}}
            />
          }

          {!success ? null : 
            <P
              lineHeight="25px"
              textAlign="center"
              color={Colors.PRIMARY}
              marginBottom={0}
              marginTop={30}
              dangerouslySetInnerHTML={{__html: success}}
            />
          }
        </form>
      </Box>
    )
  }
}

export default AuthorizeONU
