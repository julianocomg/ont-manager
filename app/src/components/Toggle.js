import React from 'react'
import {Div} from 'glamorous'
import Colors from '@utils/colors'

export default ({active, ...props}) => {
  return (
    <Div 
      background={active ? Colors.PRIMARY_DARK : null}
      color={active ? 'white' : Colors.TEXT}
      border="1px solid"
      cursor="pointer"
      borderColor={Colors.TEXT}
      padding="10px 0"
      width="50%"
      display="inline-block"
      {...props}
    />
  )
}