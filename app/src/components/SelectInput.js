import React from 'react'
import glamorous, { Div, P } from 'glamorous'
import Colors from '@utils/colors'
import Loader from 'react-loading'

const Select = glamorous.select({
  color: '#333',
  width: '100%',
  outline: 'none',
  border: 0,
  borderBottom: `1px solid ${Colors.OPAQUE}`,
  fontSize: 14,
  padding: 0,
  appearance: 'none',
  backgroundColor: 'white',
  borderRadius: 0,
  cursor: 'pointer',
  fontWeight: 300,
  paddingBottom: 4,
  marginBottom: 1,
  ':focus': {
    borderWidth: 2,
    marginBottom: 0,
    borderColor: Colors.PRIMARY_DARK
  }
})

const Label = glamorous.label({
  fontSize: 12,
  color: Colors.OPAQUE,
  top: -4,
  position: 'relative',
  pointerEvents: 'none',
  fontWeight: 400,
  transition: '.2s all'
}, ({ focused }) => {
  let color = focused ? Colors.PRIMARY_DARK : null

  return {
    color: color
  }
})

export default class SelectInput extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      focused: false
    }
  }

  render() {
    let { focused } = this.state
    let { label, loading, hideArrow, ...props } = this.props

    return (
      <Div
        marginBottom={30}
        position="relative"
        css={{
          '&::after': {
            display: loading || hideArrow ? 'none' : 'block',
            content: "''",
            position: 'absolute',
            right: 0,
            width: 0,
            top: 22,
            pointerEvents: 'none',
            height: 0,
            borderLeft: '5px solid transparent',
            borderRight: '5px solid transparent',
            borderTop: '5px solid',
            borderTopColor: focused ? Colors.PRIMARY_DARK : Colors.OPAQUE
          }
        }}>
        <Label
          focused={focused}>
          {label}
        </Label>

        <Select
          onFocus={(e) => {
            if (props.onFocus) {
              props.onFocus(e)
            }

            this.setState({ focused: true })
          }}
          onBlur={(e) => {
            if (props.onBlur) {
              props.onBlur(e)
            }

            this.setState({ focused: false })
          }}
          onChange={(e) => {
            if (props.onChange) {
              props.onChange(e)
            }
          }}
          value={props.value}
          {...props}
        />

        {!loading ? null :
          <Div
            position="absolute"
            right={0}
            bottom={6}>
            <Loader
              type="spin"
              height={16}
              width={16}
              color={Colors.OPAQUE}
            />
          </Div>
        }
      </Div>
    )
  }
}