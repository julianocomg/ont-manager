import React from 'react'
import api from '@api/ont'
import SelectInput from '@components/SelectInput'
import IoAndroidRefresh from 'react-icons/lib/io/android-refresh'
import {Div} from 'glamorous'

export default class ONUSelectInput extends React.Component {
  
  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      onus: [],
      selectedOnu: null
    }
  }

  componentDidMount() {
    this.fetchONUS()
  }

  fetchONUS() {
    this.setState({loading: true, onus: []})
    
    api.get('/onus').then(res =>{
      if (!res.ok) {
        return this.setState({loading: false, onus: []})
      }

      this.setState({
        loading: false,
        onus: res.body
      })
    })
  }

  getOnu(index) {
    return this.state.onus[Number(index)]
  }

  render() {
    let {onus, loading} = this.state

    let options = []

    if (loading) {
      options = [(<option key="loading" value="">Procurando ONUs...</option>)]
    }

    if (!onus.length && !loading) {
      options = [(<option key="empty" value="">Nenhuma ONU disponível</option>)]
    }

    if (onus.length) {
      options = onus.map((onu, i) => {
        return (
          <option key={onu.serial} value={i}>{onu.fsp} [{onu.serial}]</option>
        )
      })
    }

    return (
      <Div position="relative">
        <SelectInput 
          {...this.props}
          loading={loading}
          hideArrow={!onus.length}
          disabled={!onus.length || loading}>
          {options}
        </SelectInput>

        {(!loading && !onus.length) ?
          <Div
            position="absolute"
            cursor="pointer"
            right={-2}
            bottom={4}
            onClick={() => this.fetchONUS()}>
            <IoAndroidRefresh
              style={{fontSize: 20}}
            />
          </Div> : null}
      </Div>
    )
  }
}
