import React from 'react'
import {Div, Form, P} from 'glamorous'
import Box from '@components/Box'
import SelectInput from '@components/SelectInput'
import Colors from '@utils/colors'
import TextInput from '@components/TextInput'
import Button from '@components/Button'
import api from '@api/ont'
import serialize from 'form-serialize'

class Login extends React.Component {
  
  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      error: null,
      success: null
    }
  }

  async onSubmit(e) {
    e.preventDefault()

    let payload = serialize(e.target, {empty: true})

    this.setState({loading: true, error: null, success: null})

    api.post('/token', {
      method: 'POST',
      body: payload
    })
    .then(res => {
      if (res.error) {
        return this.setState({loading: false, error: res.error})
      }

      localStorage.setItem('token', res.body.access_token)
      window.location.reload()
    })
  }

  render() {
    let {error, success, loading} = this.state

    return (
      <Box>
        <form ref="form" onSubmit={this.onSubmit.bind(this)}>
          <TextInput
            required={true}
            name="login"
            label="Login"
          />

          <TextInput
            required={true}
            type="password"
            name="password"
            label="Senha"
          />

          <Button
            type="submit"
            loading={loading}
            disabled={loading}
            block={true}>
            Fazer Login
          </Button>

          {!error ? null : 
            <P
              lineHeight="25px"
              textAlign="center"
              color={Colors.ERROR}
              marginBottom={0}
              marginTop={30}
              dangerouslySetInnerHTML={{__html: error}}
            />
          }

          {!success ? null : 
            <P
              lineHeight="25px"
              textAlign="center"
              color={Colors.PRIMARY}
              marginBottom={0}
              marginTop={30}
              dangerouslySetInnerHTML={{__html: success}}
            />
          }
        </form>
      </Box>
    )
  }
}

export default Login
