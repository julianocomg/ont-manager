import React from 'react'
import glamorous, { Div, P } from 'glamorous'
import Colors from '@utils/colors'

const Input = glamorous.input({
  color: Colors.TEXT,
  width: '100%',
  outline: 'none',
  border: 0,
  borderBottom: `1px solid ${Colors.OPAQUE}`,
  fontSize: 14,
  padding: 0,
  fontWeight: 300,
  paddingBottom: 4,
  marginBottom: 1,
  ':focus': {
    borderWidth: 2,
    marginBottom: 0,
    borderColor: Colors.PRIMARY_DARK
  }
})

const Label = glamorous.label({
  fontSize: 14,
  color: Colors.OPAQUE,
  pointerEvents: 'none',
  fontWeight: 400,
  transition: '.2s transform',
  position: 'absolute',
}, ({ focused, hasText, forceFocus }) => {
  let color = (focused || forceFocus) ? Colors.PRIMARY_DARK : null

  return {
    color: color,
    transform: (forceFocus || focused || hasText) ? 'translateY(-18px) translateX(-5px) scale(0.8)' : ''
  }
})

export default class TextInput extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      focused: false,
      hasText: !!props.value || false
    }
  }

  render() {
    let { focused, hasText } = this.state
    let { label, forceFocus, ...props } = this.props

    return (
      <Div
        paddingTop={15}
        paddingBottom={30}
        opacity={props.disabled ? 0.5 : 1}>
        <Label
          focused={focused}
          hasText={hasText}
          forceFocus={forceFocus}>
          {label}
        </Label>

        <Input
          onFocus={(e) => {
            if (props.onFocus) {
              props.onFocus(e)
            }

            this.setState({ focused: true })
          }}
          onBlur={(e) => {
            if (props.onBlur) {
              props.onBlur(e)
            }

            this.setState({ focused: false })
          }}
          onChange={(e) => {
            if (props.onChange) {
              props.onChange(e)
            }
            
            this.setState({ hasText: !!e.target.value })
          }}
          value={props.value}
          forceFocus={forceFocus}
          {...props}
        />
      </Div>
    )
  }
}