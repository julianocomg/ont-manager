import React from 'react'
import glamorous, { Div } from 'glamorous'
import Colors from '@utils/colors'
import Loader from 'react-loading'

const Button = glamorous.button({
  backgroundColor: 'white',
  fontSize: 16,
  border: 'none',
  cursor: 'pointer',
  position: 'relative',
  display: 'inline-block',
  padding: '10px 20px',
  textAlign: 'center',
  tapHighlightColor: 'rgba(0,0,0,0) !important',
  transition: '0.25s cubic-bezier(0.17, 0.67, 0.52, 0.97)',
  borderRadius: 4,
  color: Colors.TEXT,
  letterSpacing: '1px',
  fontWeight: '400',
  boxShadow: '0 0 0 1px rgba(50, 93, 70, .1), 0 2px 5px 0 rgba(50, 93, 70, .1), 0 1px 1.5px 0 rgba(0,0,0,.07), 0 1px 2px 0 rgba(0,0,0,.08), 0 0 0 0 transparent',
  ':hover': {
    transform: 'translateY(-1px)',
    boxShadow: '0 0 0 1px rgba(50, 93, 70, .08), 0 2px 5px 0 rgba(50, 93, 70, .08), 0 3px 9px 0 rgba(50, 93, 70, .08), 0 1px 1.5px 0 rgba(0,0,0,.08), 0 1px 2px 0 rgba(0,0,0,.08)'
  },
  ':focus': { outline: 0 },
  ':active': {
    transform: 'translateY(1px)'
  },
  '& > svg': {
    top: -1,
    position: 'relative'
  }
},
  props => {
    let sizeStyle = {}

    if (props.size === 'small') {
      sizeStyle = {
        padding: '6px 10px',
        fontSize: '14px'
      }
    }

    return {
      width: props.block ? '100%' : null,
      opacity: props.disabled ? '0.6 !important' : 1,
      cursor: props.disabled ? 'not-allowed' : null,
      transform: props.disabled ? 'translateY(0px) !important' : null,
      boxShadow: props.disabled ? '0 0 0 1px rgba(50, 93, 70, .1), 0 2px 5px 0 rgba(50, 93, 70, .1), 0 1px 1.5px 0 rgba(0,0,0,.07), 0 1px 2px 0 rgba(0,0,0,.08), 0 0 0 0 transparent !important' : null,
      ...sizeStyle
    }
  }
)

export default ({ loading, children, ...props }) => {
  return (
    <Button {...props}>
      {children}

      {!loading ? null :
        <Div
          position="absolute"
          right={10}
          top={10}>
          <Loader
            type="spin"
            height={20}
            width={20}
            color={Colors.OPAQUE}
          />
        </Div>
      }
    </Button>
  )
}