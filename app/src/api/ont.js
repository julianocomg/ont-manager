import serialize from '@utils/serialize'

let BaseURL = 'http://' + window.location.hostname + ':8080'

let Api = async (method, uri, options = {}) => {
  let endpoint = BaseURL + uri

  try {
    options.method = method || 'GET'

    options.headers = {
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('token'),
      ...options.headers
    }

    if (options.params) {
      endpoint += serialize(options.params)
    }

    if (options.headers['Content-Type'] === 'application/json') {
      options.body = JSON.stringify(options.body)
    }

    if (!options.headers['Content-Type']) {
      delete options.headers['Content-Type']
    }

    let response = await fetch(endpoint, options)

    let body = await response.json()

    if (response.status === 401) {
      localStorage.removeItem('token')
      window.location.reload()
      
      return {
        ok: false,
        error: body.message
      }
    }

    if (!response.ok) {
      return {
        ok: false,
        error: body.message
      }
    }

    return {
      ok: true,
      body: body
    }
  }

  catch (e) {
    console.error(e)

    return {
      ok: false,
      error: 'Não foi possível realizar esta operação...'
    }
  }
}

export default {
  BaseURL: BaseURL,
  get: (uri, options) => Api('GET', uri, options),
  post: (uri, options) => Api('POST', uri, options),
  put: (uri, options) => Api('PUT', uri, options),
  delete: (uri, options) => Api('DELETE', uri, options)
}