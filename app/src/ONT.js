import 'babel-polyfill'
import React from 'react'
import ReactDOM from 'react-dom'
import {css} from 'glamor'
import Colors from '@utils/colors'
import {Div, H1} from 'glamorous'
import Toggle from '@components/Toggle'
import AuthorizeONU from '@components/AuthorizeONU'
import DeleteONU from '@components/DeleteONU'
import Login from '@components/Login'

css.global('html, body, #root', {
  color: Colors.TEXT,
  padding: 0,
  margin: 0,
  fontSize: 14,
  backgroundColor: '#f1f3f1',
  height: '100%',
  fontFamily: 'helvetica, sans-serif'
})

css.global('*', {
  boxSizing: 'border-box'
})

css.global('::selection', {
  background: Colors.shade(Colors.PRIMARY, 0.8)
})

class ONT extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      action: 'authorize'
    }
  }

  render() {
    let {action} = this.state

    if (!localStorage.getItem('token')) {
      return (
        <Div
        margin="auto"
        maxWidth={300}
        paddingTop={30}>
          <H1 textAlign="center" marginTop={0}>ONT Manager</H1>

          <Login />
        </Div>
      )
    }

    return (
      <Div
        margin="auto"
        maxWidth={300}
        paddingTop={30}>
        <H1 textAlign="center" marginTop={0}>ONT Manager</H1>

        <center style={{marginBottom: 20}}>
          <Toggle
            borderRadius="4px 0px 0px 4px"
            active={action === 'authorize'}
            onClick={() => this.setState({action: 'authorize'})}>
            Autorizar
          </Toggle>
          <Toggle
            borderRadius="0px 4px 4px 0"
            active={action === 'delete'}
            onClick={() => this.setState({action: 'delete'})}>
            Deletar
          </Toggle>
        </center>

        {action === 'authorize' ? <AuthorizeONU /> : null}

        {action === 'delete' ? <DeleteONU /> : null}
      </Div>
    )
  }
}

ReactDOM.render(<ONT />, document.getElementById('root'))
